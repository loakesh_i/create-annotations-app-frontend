import ApiRequests from './ApiRequests';
import dotenv from 'dotenv';
dotenv.config();

const { get, put } = new ApiRequests();

export default class ImageApis {
  getAllImagesToAnnotate = () => {
    const url =
      process.env.REACT_APP_ANNOTATIONS_TASK_API_URL + `api/get-images-to-annotate`;
    return get(url);
  };

  sendAnnotatedImageData = (id, data) => {
    const url =
      process.env.REACT_APP_ANNOTATIONS_TASK_API_URL +
      `api/update-image-with-annotations/${id}`;
    return put(url, data);
  };
}
