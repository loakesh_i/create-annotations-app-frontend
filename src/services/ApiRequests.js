import axios from 'axios';

class ApiRequests {
  get = (url) =>
    axios
      .get(url)
      .then((res) => res)
      .catch((error) => error);

  put = (url, data) =>
    axios
      .put(url, data)
      .then((res) => res)
      .catch((error) => error);
}

export default ApiRequests;
