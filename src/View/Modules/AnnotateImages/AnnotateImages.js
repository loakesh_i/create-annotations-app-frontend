import React, { Component } from 'react';
import { Navbar, Image, Nav, Card, Row, Col, Container, Form } from 'react-bootstrap';

import CanvasPane from '../../../Components/ImageEditingPane/CanvasPane';
import './AnnotateImages.css';

const AnnotateImages = ({
  title,
  dataArray,
  currentimagedata,
  handleimageclick,
  handleradiobuttonchange,
  handleannotationsdata,
  handleimagesannotationscomplete,
  handlesortbyurgency
}) => {
  console.log(`title: ${title}, currentimagedata: ${currentimagedata.annotations}`);

  return (
    <>
      <Container fluid>
        {/* Header */}
        <Navbar bg="dark" variant="dark" expand="lg">
          <Navbar.Brand href="#home">{title}</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              <Nav.Link href="#">Sort By:</Nav.Link>
              <Nav.Link href="#home" onClick={handlesortbyurgency}>Urgency</Nav.Link>
              <Nav.Link href="#link">Date Created</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        {/* Body */}
        <div className="body">
          <Row>
            {/* Left side column */}

            <Col md={3} style={{ border: '1px solid black' }}>
              <Card className="card-spacing">
                <Form>
                  <fieldset>
                    <Form.Group controlId={`form-objects`}>
                      <Form.Label as="legend">Objects</Form.Label>
                      {currentimagedata.objects_to_annotate.map((object, index) => (
                        <Form.Check
                          key={index}
                          type="radio"
                          label={object}
                          name="form-objects"
                          id={`form-objects-${index}`}
                          value={object}
                          onChange={handleradiobuttonchange}
                          defaultChecked={index === 0}
                        />
                      ))}
                    </Form.Group>
                  </fieldset>
                </Form>
              </Card>
              <Card className="card-spacing">
                <Card.Title>Up Next</Card.Title>
                <div className="d-flex flex-column justify-content-center align-items-center">
                  {dataArray.map((imageData, idx) => (
                    <Image
                      key={idx}
                      width="200"
                      height="150"
                      src={imageData.attachment}
                      className="p-1 m-1 image-side-bar"
                      onClick={() => handleimageclick(imageData)}
                    />
                  ))}
                </div>
              </Card>
            </Col>

            {/* Middle Column */}

            <Col md={6} style={{ border: '1px solid black' }}>
              <CanvasPane
                currentimagedata={currentimagedata}
                handleannotationsdata={handleannotationsdata}
                handleimagesannotationscomplete={handleimagesannotationscomplete}
              />
            </Col>

            {/* Right side column */}

            <Col md={3} style={{ border: '1px solid black' }}>
              <Card className="card-spacing">
                <Card.Title>Info</Card.Title>
                <Card.Body>
                  <div>
                    <span>Instruction: {currentimagedata.instruction}</span>
                  </div>
                  <div>
                    <span>Task Id: {currentimagedata.id}</span>
                  </div>
                  <div>
                    <span>Created At: {currentimagedata.createdAt}</span>
                  </div>
                  <div>
                    <span>With Labels: {currentimagedata.with_labels}</span>
                  </div>
                  <div>
                    <span>Urgency: {currentimagedata.urgency.toUpperCase()}</span>
                  </div>
                  <div>
                    <span>
                      Original Image:{' '}
                      {
                        <a
                          target="_blank"
                          rel="noopener noreferrer"
                          href={currentimagedata.attachment}
                        >
                          Link
                        </a>
                      }
                    </span>
                  </div>
                </Card.Body>
              </Card>
              <Card className="card-spacing">
                <Card.Title>Annotations</Card.Title>
                {currentimagedata.annotations
                  ? currentimagedata.annotations.map((annotation) => (
                      <div>
                        <div>Object: {annotation.Object}</div>
                        <div>Corner: {annotation.Corner}</div>
                        <div>Size: {annotation.Size}</div>
                      </div>
                    ))
                  : null}
              </Card>
            </Col>
          </Row>
        </div>
      </Container>
    </>
  );
};

export default AnnotateImages;
