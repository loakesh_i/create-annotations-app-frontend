import React, {
  useRef,
  useEffect,
  useCallback,
  useContext,
  useState,
  useLayoutEffect,
} from 'react';
import { Card, Button, ButtonGroup } from 'react-bootstrap';
import { fabric } from 'fabric';

import { FabricContext } from '../../FabricContext/FabricContext';

const CanvasPaneUsingHooks = ({ currentimagedata, currentlabel }) => {
  console.log('from canvasPane');
  console.log(currentimagedata);
  console.log(currentlabel);
  // const { canvas, initCanvas } = useContext(FabricContext);
  const canvasRef = useRef(null);

  const draw = useCallback(
    (canvas) => {
      console.log('draw');
      const imgURL = currentimagedata.attachment;

      const image = new Image();
      image.src = imgURL;

      canvas.setDimensions({ width: '800', height: '600' });
      // canvas.setDimensions({ width: image.width, height: image.height });
      console.log(`onload image dimensions ${image.width} ${image.height}`);
      canvas.setBackgroundImage(imgURL, canvas.renderAll.bind(canvas), {
        // should the image be resized to fit the container?
        backgroundImageStretch: false,
      });
      canvas.renderAll();
      // drawing the square boxes
      canvas.on('mouse:down', function (event) {
        mousedown(event);
      });
      canvas.on('mouse:move', function (event) {
        mousemove(event);
      });
      canvas.on('mouse:up', function (event) {
        mouseup(event);
      });

      let isDrawing = false;
      let x = 0;
      let y = 0;

      /* Mousedown */
      const mousedown = (event) => {
        console.log('mouse down');
        const mouse = canvas.getPointer(event.e);
        canvas.setCursor('crosshair');
        isDrawing = true;
        x = mouse.x;
        y = mouse.y;

        const box = new fabric.Rect({
          width: 0,
          height: 0,
          left: x,
          top: y,
          fill: 'transparent',
          stroke: 'red',
          strokeWidth: 2,
          selectable: false,

          // To keep the box drawn fixed
          flipX: false,
          flipY: false,
          // To remove the drawing borders & square controls for the box
          hasRotatingPoint: false,
          hasControls: false,
          hasBorders: false,
        });

        const text = new fabric.Text(currentlabel, {
          fontSize: 16,
          fontFamily: 'Roboto Mono',
          fontWeight: 900,
          originX: 'center',
          originY: 'center',
          fill: 'red',
          left: x + 30,
          top: y + 20,
        });

        const group = new fabric.Group([box, text]);

        canvas.add(group);
        canvas.renderAll();
        canvas.setActiveObject(box);
      };

      /* Mousemove */
      const mousemove = (event) => {
        if (!isDrawing) {
          return false;
        }

        const mouse = canvas.getPointer(event.e);

        const width = Math.abs(mouse.x - x),
          height = Math.abs(mouse.y - y);

        if (!width || !height) {
          return false;
        }

        const box = canvas.getActiveObject();
        box.set('width', width).set('height', height);
        canvas.renderAll();
      };

      /* Mouseup */
      const mouseup = (event) => {
        if (isDrawing) {
          isDrawing = false;
        }

        const box = canvas.getActiveObject();
        console.log(`box ${box}`);
        canvas.add(box);
        console.log(`getObjects ${canvas.getObjects()}`);
        canvas.renderAll();
      };

      // let getAllDrawnObjectDetails = canvas.getObjects();
      // console.log(`getAllDrawnObjectDetails ${getAllDrawnObjectDetails}`);
      // console.log(`getActiveObjects ${canvas.getActiveObjects()}`);
    },
    [currentimagedata, currentlabel]
  );

  const initCanvas = useCallback((element) => {
    const canvasOptions = {
      hoverCursor: 'pointer',
      selection: false,
      selectionBorderColor: 'green',
      backgroundColor: null,
      preserveObjectStacking: true,
    };
    let canvasObject = new fabric.Canvas(element, canvasOptions);
    canvasObject.renderAll();
  }, []);

  // useLayoutEffect(() => {
  //   initCanvas(canvasRef.current);
  //   console.log(`canvas layouteffect ${canvas}`);
  // }, []);

  const handleSubmit = () => {
    let canvas = new fabric.Canvas(canvasRef.current);
    let getAllDrawnObjectDetails = canvas.getObjects();
    console.log(`getAllDrawnObjectDetails ${getAllDrawnObjectDetails}`);
    console.log(`getActiveObjects ${canvas.getActiveObjects()}`);
    console.log('handle submit and save image');
  };

  useEffect(() => {
    const canvasOptions = {
      hoverCursor: 'pointer',
      selection: false,
      selectionBorderColor: 'green',
      backgroundColor: null,
      preserveObjectStacking: true,
    };
    let canvas = new fabric.Canvas(canvasRef.current, canvasOptions);
    draw(canvas);

    return () => canvas.clear();
  }, [draw]);

  const handleReset = () => {
    //
  };

  return (
    <Card className="card-spacing">
      <div className="d-flex flex-column">
        <div>
          <canvas className="my-2" id="canvas" ref={canvasRef}></canvas>
        </div>
        <div className="d-flex justify-content-between py-3">
          <ButtonGroup>
            <Button variant="danger">Broken</Button>
            <Button variant="warning" onClick={handleReset}>
              Reset
            </Button>
          </ButtonGroup>
          <Button variant="primary" onClick={handleSubmit}>
            Submit
          </Button>
        </div>
      </div>
    </Card>
  );
};

export default CanvasPaneUsingHooks;
