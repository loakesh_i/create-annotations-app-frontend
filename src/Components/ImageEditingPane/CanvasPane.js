import React, { Component, createRef } from 'react';
import { Card, Button, ButtonGroup, Modal, Form } from 'react-bootstrap';
import lodash from 'lodash';
import { fabric } from 'fabric';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

import ImageApis from '../../services/imageApis';
const { sendAnnotatedImageData } = new ImageApis();
toast.configure();

export default class CanvasPane extends Component {
  constructor(props) {
    super(props);
    this.canvasRef = createRef();
    this.state = {
      canvas: null,
      showErrorModal: false,
      errorMessage: '',
      canvasImage: '',
    };
  }

  tempAnnotationsArray = [];

  draw = (canvas) => {
    console.log('draw');
    const imgURL = this.state.canvasImage;
    console.log('from canvasPane');
    console.log(`imgURL ${imgURL}`);

    const image = new Image();
    image.src = imgURL;

    fabric.Image.fromURL(imgURL, (img) => {
      img.scaleToWidth(canvas.getWidth());
      img.scaleToHeight(canvas.getHeight());
      canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
        backgroundImageStretch: true,
      });
    });

    canvas.setDimensions({ width: '600', height: '400' });
    console.log(`onload image dimensions ${image.width} ${image.height}`);
    canvas.renderAll();

    // drawing the square boxes
    canvas.on('mouse:down', function (event) {
      mousedown(event);
    });
    canvas.on('mouse:move', function (event) {
      mousemove(event);
    });
    canvas.on('mouse:up', function (event) {
      mouseup(event);
    });

    let isDrawing = false;
    let x = 0;
    let y = 0;

    /* Mousedown */
    const mousedown = (event) => {
      console.log('mouse down');
      const mouse = canvas.getPointer(event.e);
      canvas.setCursor('crosshair');
      isDrawing = true;
      x = mouse.x;
      y = mouse.y;
      console.log(`finding out image x&y on mouse click x: ${x} y: ${y}`);

      const box = new fabric.Rect({
        width: 0,
        height: 0,
        left: x,
        top: y,
        fill: 'transparent',
        stroke: 'red',
        strokeWidth: 2,
        selectable: false,

        // To keep the box drawn fixed
        flipX: false,
        flipY: false,
        // To remove the drawing borders & square controls for the box
        hasRotatingPoint: false,
        hasControls: false,
        hasBorders: false,
      });

      const text = new fabric.Text(this.props.currentimagedata.currentLabel, {
        fontSize: 16,
        fontFamily: 'Roboto Mono',
        fontWeight: 900,
        originX: 'center',
        originY: 'center',
        fill: 'red',
        left: x + 30,
        top: y + 20,
      });

      const group = new fabric.Group([box, text]);

      canvas.add(group);
      canvas.renderAll();
      canvas.setActiveObject(box);
    };

    /* Mousemove */
    const mousemove = (event) => {
      if (!isDrawing) {
        return false;
      }

      const mouse = canvas.getPointer(event.e);

      const width = Math.abs(mouse.x - x),
        height = Math.abs(mouse.y - y);

      if (!width || !height) {
        return false;
      }

      const box = canvas.getActiveObject();
      box.set('width', width).set('height', height);
      canvas.renderAll();
    };

    /* Mouseup */
    const mouseup = (event) => {
      if (isDrawing) {
        isDrawing = false;
      }

      const box = canvas.getActiveObject();
      const annotationObject = {
        Object: this.props.currentimagedata.currentLabel,
        Corner: `${Math.round(x)}x${Math.round(y)}`,
        Size: `${box.width}x${box.height}`,
      };

      this.tempAnnotationsArray.push(annotationObject);
      const uniques = lodash.uniqWith(this.tempAnnotationsArray, lodash.isEqual);
      this.tempAnnotationsArray = [...uniques];
      this.props.handleannotationsdata(this.tempAnnotationsArray);
      canvas.add(box);
      canvas.renderAll();
    };
  };

  componentDidMount = () => {
    const canvasOptions = {
      hoverCursor: 'pointer',
      selection: false,
      selectionBorderColor: 'green',
      backgroundColor: null,
      preserveObjectStacking: true,
    };
    let canvas = new fabric.Canvas(this.canvasRef.current, canvasOptions);
    this.setState({
      canvas: canvas,
      canvasImage: this.props.currentimagedata.attachment,
    });
  };

  componentDidUpdate = (prevState, prevProps) => {
    console.log(
      `this.props.currentimagedata ${JSON.stringify(this.props.currentimagedata)}`
    );

    if (
      prevState.currentimagedata.attachment !== this.props.currentimagedata.attachment
    ) {
      this.setState({
        canvasImage: this.props.currentimagedata.attachment,
      });
      this.state.canvas.remove(...this.state.canvas.getObjects());
      this.tempAnnotationsArray = [];
      this.props.handleannotationsdata(this.tempAnnotationsArray);
    }
    this.draw(this.state.canvas);
  };

  handleReset = () => {
    this.state.canvas.remove(...this.state.canvas.getObjects());
    // this.state.canvas.clear();
  };

  handleSubmit = async () => {
    let resquestData;
    let toastMessage;
    if (this.state.errorMessage === '') {
      console.log(`at error message: ${this.state.errorMessage}`);
      resquestData = {
        isAnnotated: true,
        annotations: this.tempAnnotationsArray,
      };
      toastMessage = 'Image Annotated Successfully!';
    } else {
      console.log(`at error message`);
      resquestData = {
        isAnnotated: true,
        errorMessage: this.state.errorMessage,
      };
      toastMessage = 'Error Message sent Successfully!';
    }
    const result = await sendAnnotatedImageData(
      this.props.currentimagedata.id,
      resquestData
    );
    this.setState({ canvasImage: '' });
    if (result.status === 200) {
      toast.success(toastMessage, {
        position: toast.POSITION.TOP_CENTER,
      });
      this.state.canvas.clear();
      this.tempAnnotationsArray = [];
      this.props.handleannotationsdata(this.tempAnnotationsArray);
      this.props.handleimagesannotationscomplete(this.props.currentimagedata.id);
    }

    console.log(`result from backend : ${JSON.stringify(result)}`);
    console.log(
      `tempArray: ${this.tempAnnotationsArray.length} ${JSON.stringify(
        this.tempAnnotationsArray
      )}`
    );
  };

  handleErrorModalClose = () => {
    this.setState({ showErrorModal: false, errorMessage: '' });
  };

  handleErrorMessageInput = (event) => {
    this.setState({ errorMessage: event.target.value });
  };

  handleErrorModalSave = (event) => {
    this.handleSubmit();
    this.setState({ showErrorModal: false });
  };

  render() {
    return (
      <>
        <Card className="card-spacing">
          <div className="d-flex flex-column justify-content-center">
            <div>
              <canvas
                className="my-2"
                id="canvas"
                ref={this.canvasRef}
                style={{ border: '1px solid black' }}
              ></canvas>
            </div>
            <div className="d-flex justify-content-between py-3">
              <ButtonGroup>
                <Button
                  variant="danger"
                  onClick={() => {
                    this.setState({ showErrorModal: true });
                  }}
                >
                  Broken
                </Button>
                <Button variant="warning" onClick={this.handleReset}>
                  Reset
                </Button>
              </ButtonGroup>
              <Button variant="primary" onClick={this.handleSubmit}>
                Submit
              </Button>
            </div>
          </div>
        </Card>
        <Modal show={this.state.showErrorModal} onHide={this.handleErrorModalClose}>
          <Modal.Header closeButton>
            <Modal.Title>Task Id: {this.props.currentimagedata.id}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group controlId="error-message-text-area">
                <Form.Control
                  as="textarea"
                  rows={3}
                  placeholder={`Enter error message`}
                  value={this.state.errorMessage}
                  onChange={this.handleErrorMessageInput}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleErrorModalClose}>
              Close
            </Button>
            <Button variant="primary" onClick={this.handleErrorModalSave}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}
