import React, { Component } from 'react';

import AnnotateImages from './View/Modules/AnnotateImages/AnnotateImages';
import ImageApis from './services/imageApis';

const { getAllImagesToAnnotate, sendAnnotatedImageData } = new ImageApis();

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'Annotations Page',
      isLoading: true,
      dataArray: [],
      currentImageData: {
        id: '',
        instruction: '',
        callback_url: '',
        attachment_type: '',
        attachment: '',
        objects_to_annotate: [],
        with_labels: true,
        urgency: '',
        createdAt: '',
        currentLabel: '',
        annotations: [],
      },
    };
  }

  componentDidMount = async () => {
    // Make an api call and set the state of dataArray
    const result = await getAllImagesToAnnotate();
    console.log(`imagesToAnnotate data from api: ${result.data.data.length}`);
    this.setState({
      isLoading: false,
      dataArray: result.data.data,
      currentImageData: {
        id: result.data.data[0]._id,
        instruction: result.data.data[0].instruction,
        callback_url: result.data.data[0].callback_url,
        attachment_type: result.data.data[0].attachment_type,
        attachment: result.data.data[0].attachment,
        objects_to_annotate: result.data.data[0].objects_to_annotate,
        with_labels: result.data.data[0].with_labels,
        urgency: result.data.data[0].urgency,
        createdAt: result.data.data[0].createdAt,
      },
      currentLabel: '',
    });

    console.log(`current data on cdm ${this.state.currentImageData.id}`);
  };

  handleImageClick = (imageData) => {
    this.setState({
      currentImageData: {
        ...this.state.currentImageData,
        id: imageData._id,
        instruction: imageData.instruction,
        callback_url: imageData.callback_url,
        attachment_type: imageData.attachment_type,
        attachment: imageData.attachment,
        objects_to_annotate: imageData.objects_to_annotate,
        with_labels: imageData.with_labels,
        urgency: imageData.urgency,
        createdAt: imageData.createdAt,
        currentLabel: imageData.objects_to_annotate[0],
      },
    });
    console.log(`currentImageData from app.js: ${this.state.currentLabel} `);
  };

  handleRadioButtonChange = (event) => {
    console.log(`changed radio button event ${event.target.value}`);
    this.setState({
      currentImageData: {
        ...this.state.currentImageData,
        currentLabel: event.target.value,
      },
    });
  };

  handleAnnotationsData = (annotationsArray) => {
    this.setState({
      currentImageData: {
        ...this.state.currentImageData,
        annotations: annotationsArray,
      },
    });
  };

  handleImagesAnnotationsComplete = (id) => {
    const newImagesArray = [...this.state.dataArray];
    console.log(`imagesArray: ${newImagesArray.length}`);
    newImagesArray.map((element, index) => {
      if (element._id === id) {
        return newImagesArray.splice(index, 1);
      }
    });
    this.setState({ dataArray: newImagesArray });
  };

  handleSortByUrgency = (event) => {
    event.preventDefault();
    const sortedByUrgencyArray = [...this.state.dataArray];
    sortedByUrgencyArray.sort((a, b) => a.priority - b.priority);
    this.setState({ dataArray: sortedByUrgencyArray });
    this.setState({
      currentImageData: {
        id: sortedByUrgencyArray[0]._id,
        instruction: sortedByUrgencyArray[0].instruction,
        callback_url: sortedByUrgencyArray[0].callback_url,
        attachment_type: sortedByUrgencyArray[0].attachment_type,
        attachment: sortedByUrgencyArray[0].attachment,
        objects_to_annotate: sortedByUrgencyArray[0].objects_to_annotate,
        with_labels: sortedByUrgencyArray[0].with_labels,
        urgency: sortedByUrgencyArray[0].urgency,
        createdAt: sortedByUrgencyArray[0].createdAt,
      },
      currentLabel: '',
    });
  };

  render() {
    return (
      <div className="App">
        <AnnotateImages
          dataArray={this.state.dataArray}
          title={this.state.title}
          currentimagedata={this.state.currentImageData}
          handleimageclick={this.handleImageClick}
          currentlabel={this.state.currentLabel}
          handleradiobuttonchange={this.handleRadioButtonChange}
          handleannotationsdata={this.handleAnnotationsData}
          handleimagesannotationscomplete={this.handleImagesAnnotationsComplete}
          handlesortbyurgency={this.handleSortByUrgency}
        />
      </div>
    );
  }
}
