import React, { useState, createContext, useContext } from 'react';

export const AnnotationsContext = createContext();

export const AnnotationsProvider = (props) => {
  const [state, setState] = useState({
    title: 'Annotations Page',
    isLoading: true,
    dataArray: [],
    currentImageData: {
      id: '',
      instruction: '',
      callback_url: '',
      attachment_type: '',
      attachment: '',
      objects_to_annotate: [],
      with_labels: true,
      urgency: '',
      createdAt: '',
      currentLabel: '',
      annotations: [],
    },
  });

  return (
    <div>
      <AnnotationsContext.Provider value={[state, setState]}>
        {props.children}
      </AnnotationsContext.Provider>
    </div>
  );
};

export const useAnnotationsContext = () => useContext(AnnotationsContext);
