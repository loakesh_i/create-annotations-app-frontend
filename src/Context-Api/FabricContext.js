import React, { useCallback, createContext, useState } from 'react';
import { fabric } from 'fabric';

export const FabricContext = createContext([]);

export const FabricContextProvider = ({ children }) => {
  const [canvas, setCanvas] = useState(null);

  const initCanvas = useCallback((element) => {
    const canvasOptions = {
      hoverCursor: 'pointer',
      selection: false,
      selectionBorderColor: 'green',
      backgroundColor: null,
      preserveObjectStacking: true,
    };
    let canvasObject = new fabric.Canvas(element, canvasOptions);
    canvasObject.renderAll();
    setCanvas(canvasObject);
  }, []);

  return (
    <FabricContext.Provider value={{ canvas, initCanvas }}>
      {children}
    </FabricContext.Provider>
  );
};
